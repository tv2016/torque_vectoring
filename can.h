
#ifndef __CAN_H__
#define __CAN_H__

/*
 * Data structures
 */

typedef struct CANDATA {
	unsigned int sid;
	unsigned int dlc;
	unsigned int data[4];
} CANdata;


/*
 * Prototypes
 */

void CAN1_config(void);
int CAN1_send(CANdata *CANmessage);
void CAN1_handler(CANdata msg);
#endif