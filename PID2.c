#include <p30F6012A.h> 
#include <math.h>
#include "can.h"
#include "can_id.h"
#include "delay.h"


/*working variables*/

volatile unsigned long long _time;
float lastTime = 0;

volatile float errSum=0, lastErr=0;
float kp, ki, kd;

volatile float _Ref_Yaw=0;
volatile float Torque_rr=10;
volatile float Torque_rl=10;
volatile float _v_GPS = 7;
volatile float _Steering = 0.1;
volatile float _Yaw = 0.6;

//message from interface
volatile float _MaxTorque=107; //Nm


volatile unsigned int _Setpoint=0;
volatile unsigned int GyroZ=0;
volatile unsigned int CAN_write, CAN_read;
volatile unsigned int temporary;
volatile unsigned int Input;
volatile float Output=0;
volatile unsigned int OutputCAN;
volatile unsigned int CAN_torque_rl=0; // Variable to send data to CAN 
volatile unsigned int CAN_torque_rr=0; // Variable to send data to CAN
volatile unsigned int Envio=0; 


//incerteza time para evitar processamento de mensagens atrasadas
volatile unsigned int Tempo_gps[2] = 0;
volatile unsigned int Tempo_steering[2] = 0;
volatile unsigned int Tempo_imu[2] = 0;
volatile unsigned float f_incerteza = 0.5;

//Tempo esperado entre cada messagem para diferentes sensores
volatile unsigned float expected_time_gps = 0.04; //s
volatile unsigned float expected_time_imu = 0.01; //s
volatile unsigned float expected_time_steering = 0.01; //s


//
volatile unsigned int valid_cert = 0;

CANdata CANenvio;
CANdata CANtemp;

CANdata reset_message2;
CANdata reset_message;

// PIC INITIALIZATION ==========================================================
    //Clock switching and Fail-safe clock disabled
    //External clock multiplier = 4/2 = 2
    _FOSC(CSW_FSCM_OFF & HS2_PLL4);

    //Watch-dog timer off --- enabling it in software instead after initializing period
    //Prescalers make time out ~3,072 seconds (RC oscillator! may oscillate with supply voltage)
    _FWDT(WDT_OFF & WDTPSB_3 & WDTPSA_512);

    //Brown Out voltage = 2.7V & BOR circuit disabled
    //Power On Reset timer = 16ms
    ///MCLR enabled
    _FBORPOR(PBOR_OFF & PWRT_16 & MCLR_EN);

    //Code protection off
    _FGS(CODE_PROT_OFF);

    //Select comm channel
    _FICD(PGD);
//==============================================================================




void DesiredYaw()
{
    if (_v_GPS >= 5) {
     _Ref_Yaw = (_Steering*_v_GPS) / (1.59 - 0.0014*_v_GPS*_v_GPS) ;
     float temporary1 = (_Ref_Yaw*10000 + 2000);
     _Setpoint = (unsigned int)temporary1;
    }else{
        _Ref_Yaw = 0;
    }
   //Send CAN message
	CANdata reset_message;
    reset_message.sid = 151;
    reset_message.dlc = 8;
    reset_message.data[0] =  _time;
    reset_message.data[1] = _Setpoint; //2 bytes compass data 1
    reset_message.data[2] = 0; //2 bytes
    reset_message.data[3] = 0; //2 bytes
	CAN1_send(&reset_message); 
   
      //Send CAN message

    reset_message2.sid = 641;
    reset_message2.dlc = 8;
    reset_message2.data[0] =  _time;
    reset_message2.data[1] = 0; //2 bytes compass data 1
    reset_message2.data[2] = 0; //2 bytes
    reset_message2.data[3] = _v_GPS; //2 bytes
	CAN1_send(&reset_message2); 
   
    }

void SetTunings()
{
   kp = 36.39;
   ki = 4050;
   kd = 0;
}

void Compute(){
   

   SetTunings();
    
    aux_time = _time - lastTime;

    // Case where time is zero again
    if(aux_time<0){
      //lastTime = 65535 - lastTime;
    aux_time = (65535 - lastTime) + _time;    
    }
    
   float timeChange = (float)aux_time/100; //Convert from ms to s.
       //DesiredYaw();
   if(valid_cert == 0){     
        
        // Compute all the working error variables
        float error = (_Ref_Yaw - _Yaw);
        errSum += (error * timeChange);
        float dErr = (error - lastErr) / timeChange;
      
        // Compute PID Output
        Output = kp * error + ki * errSum + kd * dErr;

    }else{

      Output = 0;
      valid_cert = 0;
    } 
    
    OutputCAN = (unsigned int)Output;
       
    //timeChange  = timeChange*aux;
    Envio = (unsigned int)(errSum*1000+20000);

    CANdata reset_message;
    reset_message.sid = 914;
    reset_message.dlc = 8;
    reset_message.data[0] = _time;
    reset_message.data[1] = OutputCAN; //2 bytes compass data 1
    reset_message.data[2] = error;//2 bytes
    reset_message.data[3] = errSum; //2 bytes
    CAN1_send(&reset_message); 
  
  
    //Remember some variables for next time
    lastErr = error;
    lastTime = _time;
    // TorqueOutput();
}


//Output


void Percentagem(){
   	// PedalValue------ Valor do Pedal que o Piloto Pediu.
	//MaxTorque ------ Valor M�ximo de Torque
    // Pedal2Siemens ----- Valor de Tens�o para a siemens
    
    aux_rr  = (5 * Torque_rr) / MaxTorque; //Calcular o valor de torque em pedal 
                                           //em percentagem
    aux_rl = (5 * Torque_rl) / MaxTorque; //mesma coisa mas para a roda da esquerda
    
    aux2_rr = (aux_rr * 100) / PedalValue; //Calcular o incremento de percentagem no pedal
    aux2_rl = (aux_rl * 100) / PedalValue;
    
    PedalValue_rr = PedalValue * (1+aux2_rr); //Novo valor do pedal
    PedalValue_rl = PedalValue * (1+aux2_rl);
}

void TorqueOutput(){
   /* if (_Steering>=0 && _Steering<2.5){
        Torque_rr = (Output + Torque_rr);
        Torque_rl = (Output - Torque_rl);
        if (Torque_rr > 20){
           Torque_rr =20;
        }
        if (Torque_rl<-20){
          Torque_rl = 1;
        }
    }else{
       Torque_rr = 0;
       Torque_rl = 0;
    }
/*   
      if(_Steering<0 && _Steering>-2.5){
       Torque_rr = (Output - Torque_rr); 
       Torque_rl =  (Output + Torque_rl); 
        if (Torque_rr < -20){
           Torque_rr =1;
        }
       if (Torque_rl>20){
          Torque_rl = 20;
       }
    }else{
       Torque_rr = 0;
       Torque_rl = 0;
    }
*/
  
      if (_Steering >= 0){
        Torque_rr = (Output + Torque_rr);
        Torque_rl = (Output - Torque_rl);
        
          if (_Steering >= 0){
        Torque_rr = (Output + Torque_rr);
        Torque_rl = (Output - Torque_rl);
        if (Torque_rr > 20){
           Torque_rr = 20;
        }
        if (Torque_rl < -20){
          Torque_rl = 1;
        }
      }
      }
      
    Torque_rl = ((Torque_rl*100+10000)); //Convert to CAN info send.
    Torque_rr = ((Torque_rr*100+10000));


    CAN_torque_rr = (unsigned int)Torque_rr;
    CAN_torque_rl = (unsigned int)Torque_rl;



	// Send message to CAN.   
    CANdata reset_message;
    reset_message.sid = 913;
    reset_message.dlc = 8;
    reset_message.data[0] =  _time;
    reset_message.data[1] = _Setpoint //2 bytes compass data 1
    reset_message.data[2] = CAN_torque_rl; //2 bytes
    reset_message.data[3] = CAN_torque_rr; //2 bytes
    CAN1_send(&reset_message); 
    
}




// Initialization of TIMER.
void timer2_init(void){

    T2CONbits.TCS = 0; // internal clock (Fcy/4)
    T2CONbits.TCKPS = 1; //prescale 1:8 
    T2CONbits.TGATE = 0; // timer gated time accumulation disabled
    T2CONbits.TSIDL = 0; // continue timer in idle mode 
    
    TMR2 = 0; //initial value 
    //PR2 = 9375; //final time to make a 'cycle' of 10ms 
    PR2 = 9375;
    /*interrupts*/
    
    IPC1bits.T2IP = 6; //priority level
    IFS0bits.T2IF = 0; //clean the flag only for security
    IEC0bits.T2IE = 1; //Interrupt enable
    T2CONbits.TON = 1; // Inicia o timer 2
    
    return;
}

/********************************************
GOAL: Disable interrupts for pieces of code to be executed without being interrupted

********************************************/
int disable_interrupt()
{
    int p = SRbits.IPL;
    SRbits.IPL = 7;
    return p;
}

void set_cpu_p(int p)
{
    SRbits.IPL=p;
}


// Initialization of TIMER1
void timer1_init(void){

    T1CONbits.TCS = 0; // internal clock (Fcy/4)
    T1CONbits.TCKPS = 1; //prescale 1:8 
    T1CONbits.TGATE = 0; // timer gated time accumulation disabled
    T1CONbits.TSIDL = 0; // continue timer in idle mode 
    
    TMR1 = 0; //initial value 
    PR1 = 9375; //final time to make a 'cycle' of 10ms 
    
    /*interrupts*/
    
    IPC0bits.T1IP = 5; //priority level
    IFS0bits.T1IF = 0; //clean the flag only for security
    IEC0bits.T1IE = 1; //Interrupt enable 
    T1CONbits.TON = 1; // Inicia o timer 1
    
    return;
}


//Timer 1 interrupt
void __attribute__(( interrupt, auto_psv,shadow)) _T1Interrupt(void){
    
   // IFS0bits.T1IF = 0; // limpar a flag obrigatoriamente
    _time++; //incremento de 10ms
    IFS0bits.T1IF = 0; // limpar a flag obrigatoriamente
    
    
    return;
}


//Timer 2 interrupt
void __attribute__(( interrupt, auto_psv,shadow)) _T2Interrupt(void){
    
   
  
  DesiredYaw();  
  Compute();
  TorqueOutput();
  IFS0bits.T2IF = 0; // limpar a flag obrigatoriamente 
    

    return;
};


void __attribute__((interrupt, auto_psv)) _C1Interrupt(void){


    if(C1INTFbits.RX0IF){
          CANtemp.sid = C1RX0SIDbits.SID;
          CANtemp.dlc = C1RX0DLCbits.DLC;
          CANtemp.data[0] = C1RX0B1;
          CANtemp.data[1] = C1RX0B2;
          CANtemp.data[2] = C1RX0B3;
          CANtemp.data[3] = C1RX0B4;
          C1RX0CONbits.RXFUL = 0;
          C1INTFbits.RX0IF = 0;
       }

       if(C1INTFbits.RX1IF){
          CANtemp.sid = C1RX1SIDbits.SID;
          CANtemp.dlc = C1RX1DLCbits.DLC;
          CANtemp.data[0] = C1RX1B2;
          CANtemp.data[1] = C1RX1B2;
          CANtemp.data[2] = C1RX1B3;
          CANtemp.data[3] = C1RX1B4;
          C1RX1CONbits.RXFUL = 0;
          C1INTFbits.RX1IF = 0;
       }




       if (CANtemp.sid==150){                 //Steering Signal
          
          if(verify_uncert(CANtemp.data[0],3)){
          temporary = CANtemp.data[1];
          _Steering = (int)temporary;
          _Steering = (_Steering-2000)*((3.1415*0.1)/(float)180);
          _Steering = _Steering/4.4; // Real value in radians
          //Convert to wheel and radians
          // Tempo[i] = CANtemp.data[0];
          //]
       }else{
        valid_cert = 1;

       }
       


       if (CANtemp.sid == 640){              //Vel GPS
          if(verify_uncert(CANtemp.data[0],1)){
            temporary = CANtemp.data[3];
            _v_GPS = temporary * 0.01;
            //Tempo[1] = CANtemp.data[0];
          }else{
        valid_cert = 1;

       }
          
       }
       if (CANtemp.sid == 901){             //Yaw
            
            if(verify_uncert(CANtemp.data[0],CANtemp.sid)){
            temporary = CANtemp.data[1];
            GyroZ = (int)temporary;
            // _Yaw = (float)GyroZ *(17.5/(float)1000)*(3.1415/(float)180);
            _Yaw = (float)GyroZ *0.00175; //3.1415/180 = 0.0175 -> Conversion to radians. One extra 0 for the data.
            //Tempo[2] = CANtemp.data[0];
            }else{
        valid_cert = 1;

        }  
       }

    IFS1bits.C1IF = 0; //Limpar a flag
};

/******************************************************************************************************************
verify_uncert
Goal: verify if the time between messages is aceptable 
returns: 1 - time between messages OK
         0 - time between messages NOT OK 
*****************************************************************************************************************/

int verify_uncert(unsigned int time_temporary, unsigned int id){


  //gps
  if(id==640){
    Tempo_gps[1] = time_temporary;
    if(Tempo_gps[1]-Tempo_gps[0]<=(f_incerteza*expected_time_gps)){
      return 1;
    }else{
      return 0;
    }
    Tempo_gps[0] = Tempo_gps[1];
  }
 //imu
 if(id==901){
    Tempo_imu[1] = time_temporary;
    if(Tempo_imu[1]-Tempo_imu[0]<=(f_incerteza*expected_time_imu)){
      return 1;
    }else{
      return 0;
    }
    Tempo_imu[0] = Tempo_imu[1];
  }
  //steering
  if(id==150){
    Tempo_steering[1] = time_temporary;
    if(Tempo_steering[1]-Tempo_steering[0]<=(f_incerteza*expected_time_steering)){
      return 1;
    }else{
      return 0;
    }
    Tempo_steering[0] = Tempo_steering[1];
  } 
}

/******************************************************************************************************************
MAIN

*****************************************************************************************************************/


int main(){
    _time = 0;
    CAN1_config();
    timer1_init();
    timer2_init();


   
   while(1){
 
   }
}
