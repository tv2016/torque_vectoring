/**********************************************************************
 *   CAN IDs for FST 06e
 *
 *   log:
 *      - 18/04/2012 Miguel Silva
 *        File creation - all predicted sensors added for FST04e
 *
 *      - 29/03/2014 Bruno Santos
 *        Adjustments to the template
 *        IDs review and adaptation for new car
 *
 *		- 09/03/2015 Pedro Costa
 *		  IDs review and adaption fo FST 06e
 *   ______________________________________________________________
 *   2014 - Projecto FST Novabase
 **********************************************************************/


/**********************************************************************
 *	ID MAP - using standard IDs (lower IDs have higher priority)
 *
 *	   0			Time synchronization
 *	   1 -   99		Critical Data related to system failure
 *	 100 -  299		Unused
 *	 300 -  399		Critical sensor Data such as needed for control
 *	 400 -  599		Unused
 *	 600 -  699		Important control signals such as dashboard buttons
 *	 700 -  899		Unused
 *	 900 - 1099		Non-critical sensor data for logging purposes
 *	1100 - 1199		Unused
 *	1200 - 1299		BMS internals; non-relevant outside the battery
 *	1300 - 2047		Unused
 **********************************************************************/


#ifndef __CAN_IDS_CAN_ID_H__
#define __CAN_IDS_CAN_ID_H__


/*
 * 0
 * Time keeping
 */

/* Time synchronization */
#define CAN_ID_TIME_SYNC				0


/*
 * 1..99
 * System emergencies
 * Tractive system faults
 * Safety faults
 */

#define CAN_ID_BMS_FAULT				10
#define CAN_ID_IMD_ERROR                11
#define CAN_ID_IMD_LATCH                11

#define CAN_ID_RESET                    20

#define CAN_MOM_AIR                     99
/*
 * 200..299
 */


/*
 * 300..399
 * Vehicle control
 * Dynamics
 * Mode setting (turn on/off, change performance variables)
 */

#define CAN_ID_TPS_BPS_VALUE 130
#define CAN_ID_STEERING      150

#define CAN_ID_BPS_STATUS 	310
#define CAN_ID_BPS_VALUE  	311
#define CAN_ID_BPS_BYPASS 	312

#define CAN_ID_TV_BYPASS  	320
#define CAN_ID_TV_ACTIVATE	321

#define CAN_ID_BREAK_LIGHT 330

/* CAN_ID_BMS_CONTROL_*      350-359 (e.g. DASH, TORQUE_ENC, ENERGY_MTR, ...) */
#define CAN_ID_BMS_CONTROL_PC			350
#define CAN_ID_BMS_CONTROL_CHARGER		351
#define CAN_ID_BMS_CONTROL_DASH			352

#define CAN_ID_INTERFACE_CONTROL 360
/*
 * 400..599
 */
#define CAN_ID_DASH_BUTTONS 520
#define CAN_ID_DASH_ACK		521

/*
 * 600..699
 * Misc control
 * Feedback/input from/to non-critical systems
 */

#define CAN_ID_BMS_1_SOC			600
#define CAN_ID_BMS_1_SOH			601

#define CAN_ID_BMS_1_VOLT			602
#define CAN_ID_BMS_1_CELL_VOLT		603

#define CAN_ID_BMS_1_CELL_TEMP		604
#define CAN_ID_BMS_1_SLAVE_TEMP		605

#define CAN_ID_BMS_1_CURRENT		606

#define CAN_ID_BMS_1_OP_MODE		607

#define CAN_ID_BMS_2_SOC			608
#define CAN_ID_BMS_2_SOH			609

#define CAN_ID_BMS_2_VOLT			610
#define CAN_ID_BMS_2_CELL_VOLT		611

#define CAN_ID_BMS_2_CELL_TEMP		612
#define CAN_ID_BMS_2_SLAVE_TEMP		613

#define CAN_ID_BMS_2_CURRENT		614

#define CAN_ID_BMS_2_OP_MODE		615

#define CAN_ID_MODULE_RESET	666
#define CAN_ID_OSCFAIL 	667
#define CAN_ID_ADDRERR 	668
#define CAN_ID_STKERR	669
#define CAN_ID_MATHERR	670

#define CAN_ID_CANERROR	671


/*
 * 700..899
 */

#define CAN_ID_TE_PEDAL 800 /*Check this ID*/
#define CAN_ID_STEER 801
#define CAN_ID_TV_TCONTROL 802
/*
 * 900..1099
 * Non-critical sensor data
 * General logging
 */

#define CAN_ID_IMU_A_1		900
#define CAN_ID_IMU_G_1		901
#define CAN_ID_IMU_M_1		902

#define CAN_ID_IMU_A_2		903
#define CAN_ID_IMU_G_2		904
#define CAN_ID_IMU_M_2		905

#define CAN_ID_IMU_A_3		906
#define CAN_ID_IMU_G_3		907
#define CAN_ID_IMU_M_3		908

#define CAN_ID_DIST_R_L		909
#define CAN_ID_DIST_R_R		910
#define CAN_ID_DIST_F_L		911
#define CAN_ID_DIST_F_R		912

#define CAN_ID_PID          913
#define CAN_ID_TV           914

#define CAN_ID_MT_VEL 		950
#define CAN_ID_MT_TEMP  	951
#define CAN_ID_DCU_TEMP		960
#define CAN_ID_DCU_CURRENT	961
#define CAN_ID_WATER_1		970
#define CAN_ID_WATER_2		971



#define CAN_ID_STEERING_CALIB 980


/*
 * 1100..1199hh
 */
#define CAN_ID_TE_STATUS  	1100
#define CAN_ID_TV_STATUS  	1101
#define CAN_ID_DASH_STATUS 	1102
#define CAN_ID_DCU_STATUS 	1103
#define CAN_ID_LVB_STATUS 	1104
#define CAN_ID_WT_STATUS 	1105



/*
 * 1200..1299
 * BMS internal IDs.
 * Outside the battery they're not relevant and thus the low priority; for logging purposes only.
 * Inside the battery these are the only IDs being used and thus have as much priority as if they would start out of ID 0.
 */

#define CAN_ID_BMS_MASTER				1200
#define CAN_ID_BMS_MOM					1200
 
#define CAN_MOM_PING					1209

#define CAN_ID_BMS_QUERY				1210
#define CAN_ID_BMS_BROADCAST			1211

#define CAN_ID_BMS_SLAVE(id)			(1250 + (id))
#define CAN_ID_BMS_SLAVE_01				CAN_ID_BMS_SLAVE(0)
#define CAN_ID_BMS_SLAVE_02				CAN_ID_BMS_SLAVE(1)
#define CAN_ID_BMS_SLAVE_03				CAN_ID_BMS_SLAVE(2)
#define CAN_ID_BMS_SLAVE_04				CAN_ID_BMS_SLAVE(3)
#define CAN_ID_BMS_SLAVE_05				CAN_ID_BMS_SLAVE(4)
#define CAN_ID_BMS_SLAVE_06				CAN_ID_BMS_SLAVE(5)
#define CAN_ID_BMS_SLAVE_07				CAN_ID_BMS_SLAVE(6)
#define CAN_ID_BMS_SLAVE_08				CAN_ID_BMS_SLAVE(7)
#define CAN_ID_BMS_SLAVE_09				CAN_ID_BMS_SLAVE(8)
#define CAN_ID_BMS_SLAVE_10				CAN_ID_BMS_SLAVE(9)
#define CAN_ID_BMS_SLAVE_11				CAN_ID_BMS_SLAVE(10)
#define CAN_ID_BMS_SLAVE_12				CAN_ID_BMS_SLAVE(11)

#define CAN_ID_LVB_SLAVE				1280
#endif