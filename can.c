#include <p30F6012A.h>
#include "can.h"



/**********************************************************************
 * Name:	CAN1_config
 * Args:	-
 * Return:	-
 * Desc:	Configures CAN 1 channel.
 			(Addaped from Bruno Santos)
 **********************************************************************/
void CAN1_config(void){

	/* assuming FOSC 30MHz */

	TRISFbits.TRISF0 = 1;				/* CANRX input */
	TRISFbits.TRISF1 = 0;				/* CANTX output */

	C1CTRLbits.REQOP = 4;			/* request configuration mode */
	while(C1CTRLbits.OPMODE !=4 );	/* wait for mode to be set */

	/* control */
	C1CTRLbits.CANCAP = 0;			/* disable CAN capture */
	C1CTRLbits.CSIDL = 0;			/* continue on Idle mode */
	C1CTRLbits.CANCKS = 0;			/* master clock 4*FCY */

	/* baudrate */
	C1CFG1bits.BRP = 0;				/* TQ = 2/FCAN */
	C1CFG2bits.SEG2PHTS = 1;		/* Freely programmable */
	C1CFG2bits.PRSEG = 1;			/* propagation time segment bits length 2 x TQ */
	C1CFG1bits.SJW = 0;				/* re-synchronization jump width time is 1 x TQ */
	C1CFG2bits.SEG1PH = 7;			/* phase 1 segment length 8 x TQ */
	C1CFG2bits.SEG2PH = 4;			/* phase 2 segment length 5 x TQ <= phase1+prop.seg. && > resync jump */

	/* receive buffers */
	C1RX0CONbits.DBEN = 1;			/* RX0 full will write to RX1 */
	C1RXF0SID = 8;					/* probably both wrong (filter set but not used) and unnecessary ???!!!! */
	C1RXF1SID = 8;					/* " */
	C1RXM0SID = 0;					/* match all messages */
	C1RXM1SID = 0;					/* " */

	/* interrupts */
	C1INTEbits.RX0IE = 1;			/* RX0 interrupt enabled */
	C1INTEbits.RX1IE = 1;			/* RX1 interrupt enabled */
	IFS1bits.C1IF = 0;				/* Clear CAN1 Flag */
	IPC6bits.C1IP = 5;				/* CAN1 priority is 7 */
	IEC1bits.C1IE = 1;				/* CAN1 interrupts enabled */

	C1TX0CONbits.TXPRI = 3;
	C1RX0CONbits.RXFUL = 0;
	C1RX1CONbits.RXFUL = 0;

	C1CTRLbits.REQOP = 0;			/* request normal mode */
	while(C1CTRLbits.OPMODE!=0);	/* wait for normal mode to be set */

	return;
}


/**********************************************************************
 * Name:	CAN1_send
 * Args:	CANdata *CANmessage
 * Return:	exit status
 * Desc:	Sends CAN message.
 **********************************************************************/

int CAN1_send(CANdata *CANmessage) {

	static int try_n=0;
	unsigned int int_sid=0;

	int_sid = CANmessage->sid<<5;
	int_sid = int_sid & (0b1111100000000000);
	int_sid = int_sid | ((CANmessage->sid<<2)&(0b0000000011111100));

	if(!C1TX0CONbits.TXREQ){	/* occupied? */
		C1TX0SID = int_sid;
		C1TX0SIDbits.TXIDE = 0;
		C1TX0SIDbits.SRR = 0;
		C1TX0DLC = 0;
		C1TX0DLCbits.DLC = CANmessage->dlc;
		C1TX0B1 = CANmessage->data[0];
		C1TX0B2 = CANmessage->data[1];
		C1TX0B3 = CANmessage->data[2];
		C1TX0B4 = CANmessage->data[3];
		C1TX0CONbits.TXREQ = 1;
	}else if(!C1TX1CONbits.TXREQ){
		C1TX1SID = int_sid;
		C1TX1SIDbits.TXIDE = 0;
		C1TX0SIDbits.SRR = 0;
		C1TX1DLC = 0;
		C1TX1DLCbits.DLC = CANmessage->dlc;
		C1TX1B1 = CANmessage->data[0];
		C1TX1B2 = CANmessage->data[1];
		C1TX1B3 = CANmessage->data[2];
		C1TX1B4 = CANmessage->data[3];
		C1TX1CONbits.TXREQ = 1;
	}else if(!C1TX2CONbits.TXREQ){
		C1TX2SID = int_sid;
		C1TX2SIDbits.TXIDE = 0;
		C1TX0SIDbits.SRR = 0;
		C1TX2DLC = 0;
		C1TX2DLCbits.DLC = CANmessage->dlc;
		C1TX2B1 = CANmessage->data[0];
		C1TX2B2 = CANmessage->data[1];
		C1TX2B3 = CANmessage->data[2];
		C1TX2B4 = CANmessage->data[3];
		C1TX2CONbits.TXREQ = 1;

	}else if(try_n > 2){
		/* too many failures, clear buffers and return error */
		try_n = 0;
		C1TX0CONbits.TXREQ = 0;
		C1TX1CONbits.TXREQ = 0;
		C1TX2CONbits.TXREQ = 0;
		return -2;
	}else{
		try_n++;
		return -1;
	}
	/* reset number of tries */
	try_n = 0;
	return 0;
}