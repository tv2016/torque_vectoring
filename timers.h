/**
 * @file
 */

#ifndef __TIMERS_H__
#define	__TIMERS_H__

#ifdef TIMER_OWNER
#define EXTERN
#else
#define EXTERN extern
#endif

//Time constants
#define FCY 7500000
#define MILISEC 7500
#define MICROSEC 7.5f

/**
 * @brief      Initialize timer 1
 *
 *             Timer 1 interrupt is used to start sampling the signal
 *             periodically.
 *
 *             Calculates the sampling period (cycles) (FCY/SAMPLING_RATE) and
 *             the correction factor to correct the rounding error when casting
 *             the sampling period to an integer
 */
void timer1_init(void);

/**
 * @brief      Initialize timer 2
 *
 * Timer 2 is used has time base for the Output Compare Module (PWM)
 */
void timer2_init(void);

EXTERN volatile unsigned long TIME_MS; ///<Variable to track time since
                                       ///<initialization in milliseconds

EXTERN unsigned long samplePeriod;
EXTERN float correctionFactor;

#undef EXTERN
#endif	/* TIMERS_H */

